#!/usr/bin/env python

import rospy
from dz2_leds.msg import LEDCmd

def main():
    print('Starting mynode from mypackage')
    pub = rospy.Publisher('ledcmd', LEDCmd, queue_size=10)
    rospy.init_node('mynode')
    rate = rospy.Rate(1)

    while not rospy.is_shutdown():
        ledcmd = LEDCmd()
        ledcmd.pattern = LEDCmd.PATTERN_STROBE
        ledcmd.color.r = 255
        ledcmd.time = 0.5
        ledcmd.cycles = 1

        ledcmd.header.stamp = rospy.Time.now()

        pub.publish(ledcmd)

        rate.sleep()

if __name__=='__main__':
    main()
